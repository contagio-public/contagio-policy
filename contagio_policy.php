<?php
/**
* Plugin Name: Contagio Policy
* Plugin URI: http://contagio.pt
* Description: Plugin para Cookie Policy
* Version: 1.10
* Author: Contagio
* Author URI: http://contagio.pt
* License: A "Slug" license name e.g. GPL12
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/contagio-public/contagio-policy',
	__FILE__,
	'contagio_policy'
);

//Optional: If you're using a private repository, specify the access token like this:
//$myUpdateChecker->setAuthentication('your-token-here');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

add_shortcode( 'contagio_policy_company_name', function() {return get_option('contagio_policy_company_name');} );
add_shortcode( 'contagio_policy_company_contact', function() {return nl2br(get_option('contagio_policy_company_contact'));}  );
add_shortcode( 'contagio_policy_link', function() {return site_url() . "/cookie-privacy";} );

// create custom plugin settings menu
add_action('admin_menu', 'contagio_policy_create_menu');

function contagio_policy_create_menu() {

	//create new top-level menu
	add_menu_page('Cookie Policy', 'Cookie Policy', 'administrator', __FILE__, 'contagio_policy_settings_page' , plugins_url('/images/icon.png', __FILE__) );

	//call register settings function
	add_action( 'admin_init', 'register_contagio_policy_settings' );
}

if(isset($_POST["contagio_policy_createpage"]) && $_POST["contagio_policy_createpage"]=="1"){
	$page = get_posts( [
		'name'        => 'cookie-privacy',
		'post_type'   => 'page',
		'post_status' => 'publish',
	] );
	
	$cont=file_get_contents(dirname ( __FILE__ ) . "/policy.txt");
	
	if ( ! isset( $page[0] ) ) {
		//die();
		$res=@wp_insert_post(array(
			'post_title'   => "Politicas de Privacidade e Cookies",
			'post_content' => $cont,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'post_name'    => "cookie-privacy",
			'post_author' => get_current_user_id()
		));
	}else{
		update_option("wp_page_for_privacy_policy", $page[0]->ID);
		
		$res=@wp_update_post(array(
			"ID" =>  $page[0]->ID,
			'post_title'   => "Politicas de Privacidade e Cookies",
			'post_content' => $cont,
			'post_status'  => 'publish',
			'post_type'    => 'page',
			'post_name'    => "cookie-privacy",
			'post_author' => get_current_user_id()
		));
	}
}
function contagio_policy_add_message() {
	?>
		<div id="cookie-law-info-bar" style="z-index:99999999999; border: 0;font-size: 10pt;margin: 0 auto;padding: 16px 0;right: 0;left: 0;position: absolute;text-align: center;th: 100%;z-index: 9999;background-color: <?php echo get_option('contagio_policy_color_bg');?>; color: <?php echo get_option('contagio_policy_color_text');?>; font-family: inherit; bottom: 0px; border-top: 2px solid rgb(68, 68, 68); position: fixed;">
			<span><?php echo get_option('contagio_policy_text');?> <a href="<?php echo site_url() ?>/cookie-privacy" id="CONSTANT_OPEN_URL" target="_blank" class="cli-plugin-main-link" style="color: <?php echo get_option('contagio_policy_color_text');?>;"><?php echo get_option('contagio_policy_text_btn');?></a>
				<a href="#" id="cookie_action_close_header" class="medium cli-plugin-button cli-plugin-main-button" style="display: inline-block;
    padding: 8px 16px 8px;
    color: #fff;
    text-decoration: none;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    position: relative;
    cursor: pointer;
    margin-left: 10px;
    text-decoration: none;font-size: 13px;font-weight: 500;line-height: 1;color: <?php echo get_option('contagio_policy_color_bg');?>; background-color: <?php echo get_option('contagio_policy_color_text');?>;">OK</a>
			</span>
		</div>
		<script>
			jQuery(document).ready(function() {
				cli_show_cookiebar({
					settings: '{"animate_speed_hide":"500","animate_speed_show":"500","background":"#fff","border":"#444","border_on":true,"button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":true,"button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":false,"button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":true,"font_family":"inherit","header_fix":true,"notify_animate_hide":true,"notify_animate_show":false,"notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":false,"scroll_close_reload":false,"showagain_tab":true,"showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":false,"show_once":"10000"}'
				});
			});
		</script>
	<?php
}
add_action('wp_footer', 'contagio_policy_add_message');

add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_script( 'contagio_policy', plugin_dir_url( __FILE__ ) . 'js/client.js', array ( 'jquery' ), 1.1, false);
});

function register_contagio_policy_settings() {
	//register our settings
	register_setting( 'contagio_policy-settings-group', 'contagio_policy_company_name' );
	register_setting( 'contagio_policy-settings-group', 'contagio_policy_company_contact' );
	register_setting( 'contagio_policy-settings-group', 'contagio_policy_text' );
	register_setting( 'contagio_policy-settings-group', 'contagio_policy_text_btn' );
	register_setting( 'contagio_policy-settings-group', 'contagio_policy_color_text' );
	register_setting( 'contagio_policy-settings-group', 'contagio_policy_color_bg' );
	
}

function contagio_policy_settings_page() {
?>
	<div class="wrap">
		<h1>Contágio Policy</h1>

		<form method="post" action="options.php">
			<?php settings_fields( 'contagio_policy-settings-group' ); ?>
			<?php do_settings_sections( 'contagio_policy-settings-group' ); ?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">Nome da Empresa</th>
						<td><input type="text" name="contagio_policy_company_name" value="<?php echo esc_attr( get_option('contagio_policy_company_name') ); ?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row">Contactos da Empresa</th>
						<td>
							<textarea name="contagio_policy_company_contact"><?php echo esc_attr( get_option('contagio_policy_company_contact') ); ?></textarea>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">Texto: Ex: Utilizamos cookies para assegurar que lhe fornecemos a melhor experiência na nossa página web. Se continuar a navegar no nosso site pressupomos que aceita as nossas condições.</th>
						<td>
							<input type="text" name="contagio_policy_text" value="<?php echo esc_attr( get_option('contagio_policy_text') ); ?>" />
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">Link: Ex: Ver politica</th>
						<td>
							<input type="text" name="contagio_policy_text_btn" value="<?php echo esc_attr( get_option('contagio_policy_text_btn') ); ?>" />
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">Cor do Texto</th>
						<td><input type="text" name="contagio_policy_color_text" value="<?php echo esc_attr( get_option('contagio_policy_color_text') ); ?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row">Cor de Fundo</th>
						<td><input type="text" name="contagio_policy_color_bg" value="<?php echo esc_attr( get_option('contagio_policy_color_bg') ); ?>" /></td>
					</tr>
					<?php 
						$page = get_posts( [
							'name'        => 'cookie-privacy',
							'post_type'   => 'page',
							'post_status' => 'publish',
						] );
						
						if ( ! isset( $page[0] ) ) {
							?>
								<tr valign="top">
									<th scope="row">Criar Página de Politicas de Privacidade?</th>
									<td><input type="checkbox" name="contagio_policy_createpage" value="1" /></td>
								</tr>
							<?php
						}else{
							?>
								<tr valign="top">
									<th scope="row"><a href="<?php echo get_edit_post_link( $page[0]->ID); ?>">Ver página de privacidade</a></th>
									<td></td>
								</tr>
								<tr valign="top">
									<th scope="row">RECRIAR Página de Politicas de Privacidade?</th>
									<td><input type="checkbox" name="contagio_policy_createpage" value="1" /></td>
								</tr>
							<?php
						}
					?>
				</table>
			<?php submit_button(); ?>
		</form>
	</div>
<?php }